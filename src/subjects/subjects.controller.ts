import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { SubjectsService } from './subjects.service';
import { Subject } from 'src/schemas/subject.schemas';

@Controller('subjects')
export class SubjectsController {
  constructor(private readonly subjectService: SubjectsService) {}

  @Post()
  async registerSubject(@Body() subject: Subject): Promise<Subject> {
    return this.subjectService.registerSubject(subject);
  } // End of registerSubject

  @Get()
  async getSubjects(): Promise<Subject[]> {
    return this.subjectService.getSubjects();
  } // End of getSubjects

  @Get('get-subject-max-grade')
  async getSubjectsMaxGrades(): Promise<{}[]> {
    return this.subjectService.getSubjectMaxGrade();
  }

  @Get(':id')
  async getSpecificSubject(@Param('id') id: string): Promise<Subject> {
    return this.subjectService.getSpecificSubject(id);
  } // End of getSpecificSubject

  @Put(':id')
  async updateSubjectInfo(
    @Param('id') id: string,
    @Body() student: Subject,
  ): Promise<Subject> {
    return this.subjectService.updateSubjectInfo(id, student);
  } // End of updateSubjectInfo

  @Delete(':id')
  async deleteSubject(@Param('id') id: string) {
    return this.subjectService.deleteSubject(id);
  } // End of deleteStudent
} // End of deleteSubject
