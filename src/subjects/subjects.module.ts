import { Module } from '@nestjs/common';
import { SubjectsController } from './subjects.controller';
import { SubjectsService } from './subjects.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SubjectSchema } from 'src/schemas/subject.schemas';
import { StudentAssignmentSchema } from 'src/schemas/student_assignment.schemas';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Subject',
        schema: SubjectSchema,
      },
      {
        name: 'StudentAssignment',
        schema: StudentAssignmentSchema,
      },
    ]),
  ],
  controllers: [SubjectsController],
  providers: [SubjectsService],
})
export class SubjectsModule {}
