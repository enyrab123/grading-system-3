import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Types } from 'mongoose';
import { Subject } from 'src/schemas/subject.schemas';
import { StudentAssignment } from 'src/schemas/student_assignment.schemas';

@Injectable()
export class SubjectsService {
  constructor(
    @InjectModel(Subject.name) private subjectModel: mongoose.Model<Subject>,
    @InjectModel(StudentAssignment.name)
    private assignmentModel: mongoose.Model<StudentAssignment>,
  ) {}

  async registerSubject(subject: Subject): Promise<Subject> {
    if (!subject.subjectCode || !subject.subjectName) {
      throw new BadRequestException(
        'Subject code and subject name is required.',
      );
    }
    try {
      const existingSubject = await this.subjectModel.findOne({
        subjectCode: subject.subjectCode,
        subjectName: subject.subjectName,
      });

      if (existingSubject) {
        throw new ConflictException(
          'Subject with the provided subject code and subject name already exists.',
        );
      }

      const result = await this.subjectModel.create(subject);

      return result;
    } catch (error) {
      if (error instanceof ConflictException) {
        throw error; // Re-throw known exceptions
      }
      console.log(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of registerSubject

  async getSubjects(): Promise<Subject[]> {
    try {
      const result = await this.subjectModel.find();

      this.checkResult(result);

      return result;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of getSubjects

  async getSpecificSubject(id: string): Promise<Subject> {
    try {
      const result = await this.subjectModel.findById(id);

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      }
      console.log(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of getSpecificSubject

  async updateSubjectInfo(id: string, subject: Subject): Promise<Subject> {
    if (!subject.subjectCode || !subject.subjectName) {
      throw new BadRequestException(
        'Subject code and subject name is required.',
      );
    }

    try {
      const result = await this.subjectModel.findByIdAndUpdate(id, subject, {
        new: true,
        runValidators: true,
      });

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      }
      console.log(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of updateSubjectInfo

  async deleteSubject(id: string): Promise<Subject> {
    try {
      await this.subjetHaveAssignment(id);

      const result = await this.subjectModel.findByIdAndDelete(id);

      this.checkResult(result);

      return result;
    } catch (error) {
      if (
        error instanceof ConflictException ||
        error instanceof NotFoundException
      ) {
        throw error; // Re-throw known exceptions
      }
      console.log(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of deleteSubject

  async getSubjectMaxGrade(): Promise<{}[]> {
    try {
      const result = await this.assignmentModel.aggregate([
        {
          $group: {
            _id: '$subjectCode',
            highestGrade: { $max: '$grade' },
          },
        },
        {
          $lookup: {
            from: 'subjects',
            localField: '_id',
            foreignField: 'subjectCode',
            as: 'subject',
          },
        },
        {
          $unwind: '$subject',
        },
        {
          $project: {
            _id: 0,
            subjectCode: '$_id',
            subjectName: '$subject.subjectName',
            highestGrade: 1,
          },
        },
      ]);

      return result;
    } catch (error) {
      console.error(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of getSubjectMaxGrade

  private async subjetHaveAssignment(id: string): Promise<void> {
    const getSubject = await this.subjectModel.findById(id);

    // Check if getSubject is null
    if (!getSubject) {
      throw new NotFoundException('Subject not found');
    }

    const result = await this.assignmentModel.findOne({
      subjectCode: getSubject.subjectCode,
    });

    if (result) {
      throw new ConflictException(
        'Cannot delete subject, subject has existing assignments',
      );
    }
  } // End of subjetHaveAssignment

  private checkResult(result: Subject | Subject[] | null): void {
    if (!result || (Array.isArray(result) && result.length === 0)) {
      throw new NotFoundException('Subject not found.');
    }
  } // End of checkResult
} // End of SubjectsService
