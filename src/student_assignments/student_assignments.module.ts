import { Module } from '@nestjs/common';
import { StudentAssignmentsController } from './student_assignments.controller';
import { StudentAssignmentsService } from './student_assignments.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StudentAssignmentSchema } from 'src/schemas/student_assignment.schemas';
import { Student } from 'src/schemas/student.schemas';
import { Subject } from 'src/schemas/subject.schemas';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'StudentAssignment',
        schema: StudentAssignmentSchema,
      },
      {
        name: 'Student',
        schema: Student,
      },
      {
        name: 'Subject',
        schema: Subject,
      },
    ]),
  ],
  controllers: [StudentAssignmentsController],
  providers: [StudentAssignmentsService],
})
export class StudentAssignmentsModule {}
