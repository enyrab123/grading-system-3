import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Student } from 'src/schemas/student.schemas';
import { Subject } from 'src/schemas/subject.schemas';
import { StudentAssignment } from 'src/schemas/student_assignment.schemas';
import { ObjectId } from 'mongoose';

@Injectable()
export class StudentAssignmentsService {
  constructor(
    @InjectModel(StudentAssignment.name)
    private assignmentModel: mongoose.Model<StudentAssignment>,
    @InjectModel(Student.name)
    private studentModel: mongoose.Model<Student>,
    @InjectModel(Subject.name)
    private subjectModel: mongoose.Model<Subject>,
  ) {}

  async assignSubject(
    studentAssignment: StudentAssignment,
  ): Promise<StudentAssignment> {
    if (!studentAssignment.studentId || !studentAssignment.subjectCode) {
      throw new BadRequestException('Student id and subject code is required.');
    }
    try {
      await this.checkStudentExist(studentAssignment.studentId);
      await this.checkSubjectExist(studentAssignment.subjectCode);
      await this.checkAssignmentDetailsExist(
        studentAssignment.studentId,
        studentAssignment.subjectCode,
      );

      const result = await this.assignmentModel.create(studentAssignment);

      return result;
    } catch (error) {
      if (
        error instanceof NotFoundException ||
        error instanceof ConflictException
      ) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of assignStudent

  async getAssignments(): Promise<StudentAssignment[]> {
    try {
      const result = await this.assignmentModel.find();

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of getAssignments

  async updateAssignment(
    // Check error checking

    studentAssignment: StudentAssignment,
    id: string,
  ): Promise<StudentAssignment> {
    const studentId = studentAssignment.studentId;
    const subjectCode = studentAssignment.subjectCode;

    if (!studentId || !subjectCode) {
      throw new BadRequestException('Student id and subject code is required.');
    }
    try {
      await this.checkAssignmentExist(id);
      await this.checkStudentExist(studentId);
      await this.checkSubjectExist(subjectCode);
      await this.checkAssignmentDetailsExist(studentId, subjectCode);

      const result = await this.assignmentModel.findByIdAndUpdate(
        id,
        studentAssignment,
        {
          new: true,
          runValidators: true,
        },
      );

      this.checkResult(result);

      return result;
    } catch (error) {
      if (
        error instanceof NotFoundException ||
        error instanceof ConflictException
      ) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of editAssignments

  async deleteAssignment(subjectId: string): Promise<StudentAssignment> {
    try {
      const result = await this.assignmentModel.findByIdAndDelete(subjectId);

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of deleteAssignment

  async updateGrade(
    studentAssignment: StudentAssignment,
    id: string,
  ): Promise<StudentAssignment> {
    const grade: number = studentAssignment.grade;

    if (!grade) {
      throw new BadRequestException('Student grade is required.');
    }

    try {
      const result = await this.assignmentModel.findByIdAndUpdate(
        id,
        studentAssignment,
        {
          new: true,
          runValidators: true,
        },
      );

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of updateGrade

  private async checkStudentExist(studentId: string) {
    try {
      const studentExist = await this.studentModel.findById(studentId);

      if (!studentExist) {
        throw new NotFoundException('Student not found.');
      }
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of checkStudentExist

  private async checkSubjectExist(subjectCode: string): Promise<void> {
    try {
      const subjectExist = await this.subjectModel.findOne({
        subjectCode: subjectCode,
      });

      if (!subjectExist) {
        throw new NotFoundException('Subject not found.');
      }
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of checkSubjectExist

  private async checkAssignmentExist(subjectId: string): Promise<void> {
    const isAssignmentExist = await this.assignmentModel.findById(subjectId);

    if (!isAssignmentExist) {
      throw new ConflictException('Assignment not found.');
    }
  } // End of checkAssignmentExist

  private async checkAssignmentDetailsExist(
    studentId: string,
    subjectCode: string,
  ): Promise<void> {
    const isAssignmentDetailsExist = await this.assignmentModel.findOne({
      studentId: studentId,
      subjectCode: subjectCode,
    });

    if (isAssignmentDetailsExist) {
      throw new ConflictException('Subject assignment already exist.');
    }
  } // End of checkExistingAssignment

  private checkResult(
    result: StudentAssignment | StudentAssignment[] | null,
  ): void {
    if (!result || (Array.isArray(result) && result.length === 0)) {
      throw new NotFoundException('No assignments found.');
    }
  } // End of checkResult
} // End of StudentAssignmentsService
