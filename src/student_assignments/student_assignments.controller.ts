import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { StudentAssignmentsService } from './student_assignments.service';
import { StudentAssignment } from 'src/schemas/student_assignment.schemas';

@Controller('assignments')
export class StudentAssignmentsController {
  constructor(
    private readonly studentAssignmentService: StudentAssignmentsService,
  ) {}

  @Post('assign-subject')
  async assignSubject(
    @Body() assignment: StudentAssignment,
  ): Promise<StudentAssignment> {
    return this.studentAssignmentService.assignSubject(assignment);
  } // End of assignStudent

  @Get()
  async getAssignments(): Promise<StudentAssignment[]> {
    return this.studentAssignmentService.getAssignments();
  } // End of getAssignments

  @Put(':id')
  async editAssignment(
    @Param('id') id: string,
    @Body() assignment: StudentAssignment,
  ): Promise<StudentAssignment> {
    return this.studentAssignmentService.updateAssignment(assignment, id);
  } // End of editAssignment

  @Delete(':id')
  async deleteAssignment(@Param('id') id: string): Promise<StudentAssignment> {
    return this.studentAssignmentService.deleteAssignment(id);
  } // End of deleteAssignment

  @Put('update-grade/:id')
  async updateGrade(
    @Param('id') id: string,
    @Body() assignment: StudentAssignment,
  ): Promise<StudentAssignment> {
    return this.studentAssignmentService.updateGrade(assignment, id);
  } // End of updateGrade
} // End of StudentAssignmentsController {
