import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { ObjectId, Types } from 'mongoose';
import { Student } from 'src/schemas/student.schemas';
import { StudentAssignment } from 'src/schemas/student_assignment.schemas';

@Injectable()
export class StudentsService {
  constructor(
    @InjectModel(Student.name)
    private studentModel: mongoose.Model<Student>,
    @InjectModel(StudentAssignment.name)
    private assignmentModel: mongoose.Model<StudentAssignment>,
  ) {}

  async registerStudent(student: Student): Promise<Student> {
    if (!student.firstname || !student.lastname) {
      throw new BadRequestException('Firstname and lastname is required.');
    }
    try {
      await this.checkStudentAlreadyExist(student.firstname, student.lastname);

      const result = await this.studentModel.create(student);

      return result;
    } catch (error) {
      if (error instanceof ConflictException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of registerStudent

  async getStudents(): Promise<Student[]> {
    try {
      const result = await this.studentModel.find();

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of getStudents

  async getSpecificStudent(id: string): Promise<Student> {
    try {
      const result = await this.studentModel.findById(id);

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of getSpecificStudent

  async updateStudentInfo(id: string, student: Student): Promise<Student> {
    if (!student.firstname || !student.lastname) {
      throw new BadRequestException('Firstname and lastname is required.');
    }
    try {
      const result = await this.studentModel.findByIdAndUpdate(id, student, {
        new: true,
        runValidators: true,
      });

      this.checkResult(result);

      return result;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of updateStudentInfo

  async deleteStudent(id: string): Promise<Student> {
    try {
      await this.studentHaveAssignment(id);

      const result = await this.studentModel.findByIdAndDelete(id);

      this.checkResult(result);

      return result;
    } catch (error) {
      if (
        error instanceof ConflictException ||
        error instanceof NotFoundException
      ) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  } // End of deleteStudent

  async viewSpecificStudentInfo(studentId: string): Promise<{}> {
    try {
      const objectStudentId = new Types.ObjectId(studentId);
      const result = await this.studentModel.aggregate([
        {
          $match: { _id: objectStudentId }, // Match the specific student by ID
        },
        {
          $lookup: {
            from: 'studentassignments', // The name of the student assignments collection
            localField: '_id',
            foreignField: 'studentId',
            as: 'assignments', // Join results into this field
          },
        },
        {
          $unwind: { path: '$assignments', preserveNullAndEmptyArrays: true },
        },
        {
          $group: {
            _id: '$_id',
            firstname: { $first: '$firstname' },
            lastname: { $first: '$lastname' },
            subjects: {
              $push: {
                subjectCode: '$assignments.subjectCode',
                grade: '$assignments.grade',
              },
            },
          },
        },
        {
          $project: {
            _id: 0, // Exclude the MongoDB _id field
            name: { $concat: ['$firstname', ' ', '$lastname'] },
            subjects: 1, // Include subjects array
          },
        },
      ]);

      if (result.length === 0) {
        throw new NotFoundException('No assignments found.');
      }

      return result[0];
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw error; // Re-throw known exceptions
      } else {
        // Log or handle unexpected errors
        console.log(error);
        throw new InternalServerErrorException('An unexpected error occurred.');
      }
    }
  }

  async getMinMaxGrade(): Promise<{}[]> {
    try {
      const result = await this.assignmentModel.aggregate([
        {
          $group: {
            _id: '$studentId',
            highestGrade: { $max: '$grade' },
            lowestGrade: { $min: '$grade' },
          },
        },
        {
          $lookup: {
            from: 'students',
            localField: '_id',
            foreignField: '_id',
            as: 'student',
          },
        },
        {
          $unwind: '$student',
        },
        {
          $project: {
            _id: 0,
            name: { $concat: ['$student.firstname', ' ', '$student.lastname'] },
            highestGrade: 1,
            lowestGrade: 1,
          },
        },
      ]);

      return result;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException('An unexpected error occurred.');
    }
  } // End of getMinMaxGrade

  private async studentHaveAssignment(studentId: string): Promise<void> {
    const result = await this.assignmentModel.findOne({
      studentId: studentId,
    });

    if (result) {
      throw new ConflictException(
        'Cannot delete student, student has existing assignments',
      );
    }
  } // End of studentHaveAssignment

  private checkResult(result: Student | Student[] | null): void {
    console.log(result);
    if (!result) {
      throw new NotFoundException('Student not found.');
    }
  } // End of checkResult

  private async checkStudentAlreadyExist(firstname: string, lastname: string) {
    const existingStudent = await this.studentModel.findOne({
      firstname: firstname,
      lastname: lastname,
    });

    if (existingStudent) {
      throw new ConflictException(
        'Student with the provided first name and last name already exists.',
      );
    }
  } // End of checkStudentAlreadyExist
} // End of StudentsService
