import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { StudentsService } from './students.service';
import { Student } from 'src/schemas/student.schemas';
import { StudentAssignment } from 'src/schemas/student_assignment.schemas';
import { ObjectId } from 'mongoose';

@Controller('students')
export class StudentsController {
  constructor(private readonly studentService: StudentsService) {}

  @Post()
  async registerStudent(@Body() student: Student): Promise<Student> {
    return this.studentService.registerStudent(student);
  } // End of registerStudent

  @Get()
  async getStudents(): Promise<Student[]> {
    return this.studentService.getStudents();
  } // End of getStudents

  @Get('/get-min-max-grades')
  async getMinMaxGrade(): Promise<{}[]> {
    return this.studentService.getMinMaxGrade();
  } // End of getMinMaxGrade

  @Get('/view-subject-grades/:id')
  async viewSpecificStudentInfo(@Param('id') studentId: string): Promise<{}> {
    return this.studentService.viewSpecificStudentInfo(studentId);
  } // End of viewSpecificStudentInfo

  @Get(':id')
  async getSpecificStudent(@Param('id') id: string): Promise<{}> {
    return this.studentService.getSpecificStudent(id);
  } // End of getSpecificStudent

  @Put(':id')
  async updateStudentInfo(
    @Param('id') id: string,
    @Body() student: Student,
  ): Promise<Student> {
    return this.studentService.updateStudentInfo(id, student);
  } // End of updateStudentInfo

  @Delete(':id')
  async deleteStudent(@Param('id') id: string) {
    return this.studentService.deleteStudent(id);
  } // End of deleteStudent
} // End of StudentsController
