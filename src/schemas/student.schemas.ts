import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
  timestamps: true,
})
export class Student {
  @Prop()
  firstname: string;

  @Prop()
  lastname: string;
}

export const StudentSchema = SchemaFactory.createForClass(Student);
