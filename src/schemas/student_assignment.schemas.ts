import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({
  timestamps: true,
})
export class StudentAssignment {
  @Prop()
  studentId: string;

  @Prop()
  subjectCode: string;

  @Prop({ default: null })
  grade: number;
}

export const StudentAssignmentSchema =
  SchemaFactory.createForClass(StudentAssignment);
